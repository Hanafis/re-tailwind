import CardBadage from "./Card/CardBadage";
import CardContainer from "./Card/CardContainer";

function ContentThreeBadges(props) {
  return (
    <div>
      <div className="container mt-5">
        <div className="row mb-3">
          <div className="">3-column with badges</div>
          <div className="ml-2 ">
            <div className="btn-grey float-left border-grey">PNG PREVIEW</div>
          </div>
          <div className="col ">
            <a href="/pricing" className="text-primary float-right">
              Get the code →
            </a>
          </div>
        </div>
      </div>
      <CardContainer>
        <div className="ml-2 mb-2 mt-5 mb-5">
          <div className="row ml-2 ">
            <h2 className="bold">Recent publications</h2>
          </div>
          <div className="row center-row ml-2 ">
            <div className=" col">
              <span className="text-grey">
                Nullam risus blandit ac aliquam justo ipsum. Quam mauris
                volutpat massa dictumst amet. Sapien tortor lacus arcu.
              </span>
            </div>
          </div>
          <div className="br-b mt-4"></div>
          <CardBadage posts={props.posts} deleteData={props.deleteData} onGetData={props.getIdData} users={props.users} />
          <button className="btn btn-primary mt-2 ml-3" onClick={props.onBtnTambah}>Tambah</button>
        </div>
      </CardContainer>
    </div>
  );
}
export default ContentThreeBadges;
