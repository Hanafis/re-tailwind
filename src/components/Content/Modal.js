
function Modal(props) {
  const onUpdate=(event)=>{
    event.preventDefault();
    const data ={
      userId: props.post.userId, 
      id: props.post.id, 
      title:event.target.title.value,
      body:event.target.body.value,
    }
    props.updateData(data)
    props.onCloseModal()
  }
  return (
    <div id={"demo-modal"} className={"modal-" + props.modal}>
      <div className="modal__content">
        <h2>Edit Content</h2>
        <form onSubmit={onUpdate}>
          <div className="container">
            <div className="row">
              <div className="col">
                <input
                  name="title"
                  defaultValue={props.post.title}
                  className="form-control"
                />
              </div>
            </div>
            <div className="row">
              <div className="col">
                <textarea
                  name="body"
                  className="form-control mt-2" style={{width: "412px", height: "317px"}}
                  defaultValue={props.post.body}
                ></textarea>
              </div>
            </div>
          </div>
          <div className="modal__footer">
            <div className="container">
              <div className="row">
                <div className="col">
                  <button className="btn btn-primary mt-2 float-left btn-responsive">
                    Save
                  </button>
                </div>
                <div className="col">
                  <button className="btn btn-danger mt-2 btn-responsive" onClick={props.onCloseModal}>
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
export default Modal;
