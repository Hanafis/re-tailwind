import { useState } from "react";
function ModalTambah(props){
  
  const onSaveData=(event)=>{
    event.preventDefault();
    let userId =Math.floor(Math.random() * 10) + 1
    let id=Math.floor(Math.random() * 100) + 1
    console.log(userId)
    const data ={
      userId: userId, 
      id: id, 
      title:event.target.title.value,
      body:event.target.body.value,
    }
    event.target.title.value=''
    event.target.body.value=''
    props.tambahData(data)
    props.onCloseModalTambah()
  }
    return(
        <div id={"modal-tambah"} className={"modal-" + props.ModalTambah}>
      <div className="modal__content">
        <h2>Tambah Content</h2>
        <form onSubmit={onSaveData}>
          <div className="container">
            <div className="row">
              <div className="col">
                <input
                  name="title"
                  placeholder="Title"
                  className="form-control"
                />
              </div>
            </div>
            <div className="row">
              <div className="col">
                <textarea
                  name="body"
                  placeholder="Content"
                  style={{width: "412px", height: "317px"}}
                  className="form-control mt-2"
                ></textarea>
              </div>
            </div>
          </div>
          <div className="modal__footer">
            <div className="container">
              <div className="row">
                <div className="col">
                  <button className="btn btn-primary mt-2 float-left btn-responsive">
                    Tambah
                  </button>
                </div>
                <div className="col">
                  <button className="btn btn-danger mt-2 btn-responsive" onClick={props.onCloseModalTambah}>
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    )
}
export default ModalTambah;