import CardContainer from "./Card/CardContainer";
import CardImage from "./Card/CardImage";

function ContentThreeCard(props) {
  return (
    <div>
      <div className="container mt-5">
        <div className="row mb-3">
          <div className="">3-column cards</div>
          <div className="ml-2 ">
            <div className="btn-grey float-left border-grey">PNG PREVIEW</div>
          </div>
          <div className="col ">
            <a href="/pricing" className="text-primary float-right">
              Get the code →
            </a>
          </div>
        </div>
      </div>
      <CardContainer>
        <div className="ml-2 mb-2 mt-5 mb-5">
          <center>
            <h2 className="bold">From The Blog</h2>
            <span className="text-grey">
              <div className="col-6">
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsa
                libero labore natus atque, ducimus sed.
              </div>
            </span>
          </center>
          <CardImage posts={props.posts} deleteData={props.deleteData} onGetData={props.getIdData} users={props.users}/>
          <button className="btn btn-primary mt-2 ml-3" onClick={props.onBtnTambah}>Tambah</button>
        </div>
      </CardContainer>
    </div>
  );
}
export default ContentThreeCard;
