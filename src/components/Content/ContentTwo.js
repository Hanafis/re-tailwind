
import Card from "./Card/Card";
import CardContainer from "./Card/CardContainer";


function ContentTwo(props) {
  return (
    <div>
      <div className="container mt-5">
        <div className="row mb-3">
          <div className=""><span>2-column with sign-up</span></div>
          <div className="ml-2 ">
            <div className="btn-grey float-left border-grey"><span>PNG PREVIEW</span></div>
          </div>
          <div className="col ">
            <a href="/pricing" className="text-primary float-right">
              Get the code →
            </a>
          </div>
        </div>
      </div>
      <CardContainer>
        <div className="ml-2 mb-2 mt-5 mb-5">
          <div className="row ml-3">
            <h2 className="bold">Press</h2>
          </div>
          <div className="row center-row ml-2">
            <div className=" col-6 float-right">
              <h3 className="text-grey mt-0">
                Get weekly articles in your inbox on how to grow your business.
              </h3>
            </div>
            <div className="col">
              <div className="row center-row mr-3 float-right">
                <div>
                  <input
                    type="text"
                    className="mr-2 mt-2 search center-row"
                    size=""
                    placeholder="Enter your email"
                  />
                </div>
                <div>
                  <button className="btn mt-2 ml-2 btn-primary center-row">Notify Me</button>
                </div>
              </div>
            </div>
          </div>
          <div className="br-b mt-4"></div>
          <Card posts={props.posts} deleteData={props.deleteData} onGetData={props.getIdData} users={props.users}/>
          <button className="btn btn-primary mt-2 ml-3" onClick={props.onBtnTambah}>Tambah</button>
        </div>
      </CardContainer>
    </div>
  );
}
export default ContentTwo;
