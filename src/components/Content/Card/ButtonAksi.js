function ButtonAksi(props) {
    const onClickEdit=()=>{
        props.onGetData(props.post)
      }
    const onDeleteData=()=>{
      props.deleteData(props.post)
    }
  return (
    <div className="row edit-btn mt-2">
      <div className="col-6 ">
        <button className="btn btn-primary btn-responsive" onClick={onClickEdit}>
          Edit
        </button>
      </div>
      <div className="col-6">
        <button className="btn btn-danger float-right btn-responsive" onClick={onDeleteData}>Hapus</button>
      </div>
    </div>
  );
}
export default ButtonAksi;
