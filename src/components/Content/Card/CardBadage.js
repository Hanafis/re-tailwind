import ButtonAksi from "./ButtonAksi";
import CardProfile from "./CardProfile";
function CardBadage(props){
    return(
        <div className="row m-2 ">
            {props.posts
              ? props.posts.map((d) => (
                  <div className="col-xs-12 col-sm-4 mt-2 mb-2 edit" key={d.id}>
                    <span className="badge badge-round badge-purple">
                      Primary
                    </span>
                    <h3 className="mt-0 bold mt-2">{d.title}</h3>
                    <p className="text-grey mt-0">{d.body}</p>
                    <CardProfile posts={d} users={props.users}/>
                    <ButtonAksi onGetData={props.onGetData} deleteData={props.deleteData} post={d}/>
                  </div>
                ))
              : "loading"}
          </div>
    )
    
}
export default CardBadage;