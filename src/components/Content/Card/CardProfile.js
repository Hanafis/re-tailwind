function CardProfile(props) {
  return (
    <div className="row ml-1">
      <div className="col-2">
        <img src="avatar.png" className="img-fluid" width={35} />
      </div>

      {props.users
        ? props.users.map((s) => {
            if (props.posts.userId == s.id) {
              return (
                <div className="col ml-2" key={s.id}>
                  <div className="bold">{s.name}</div>
                  <h5 className="text-grey mt-0">Mar 16, 2020 - 6 min read</h5>
                </div>
              );
            }
          })
        : "loading"}
    </div>
  );
}
export default CardProfile;
