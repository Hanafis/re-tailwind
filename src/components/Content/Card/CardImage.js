import CardProfile from "./CardProfile";
import ButtonAksi from "./ButtonAksi";
function CardImage(props) {
  return (
    <div className="row m-2">
      {props.posts
        ? props.posts.map((d) => (
            <div
              className="col-xs-12 col-md-4 col-sm-6 mt-2 mb-2 edit"
              key={d.id}
            >
              <div className="card">
                <img className="card-img-top" src="/sample.jpeg" />
                <div className="card-body">
                  <h5 className="card-title color-primary">Article</h5>
                  <h3 className="bold mt-0">{d.title}</h3>
                  <p>{d.body}</p>
                  <CardProfile posts={d} users={props.users} />
                  <ButtonAksi onGetData={props.onGetData} deleteData={props.deleteData} post={d} />
                </div>
              </div>
            </div>
          ))
        : "loading"}
    </div>
  );
}
export default CardImage;
