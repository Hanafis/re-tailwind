import ButtonAksi from "./ButtonAksi";

function Card(props) {
  return (
    <div className="row m-2">
      {props.posts
        ? props.posts.map((d) => (
            <div className="col-xs-12 col-sm-6 mt-2 mb-2 edit" key={d.id}>
              {props.users
                ? props.users.map((s) => {
                    if (d.userId == s.id) {
                      return <h5 key={s.id} className="text-grey ">{s.name}</h5>;
                    }
                  })
                : "loading"}

              <h3 className="mt-0 bold">{d.title}</h3>
              <p className="text-grey mt-0">{d.body}</p>
              <a href="" className="bold">
                Read Full Story
              </a>
              <ButtonAksi onGetData={props.onGetData} deleteData={props.deleteData} post={d} />
            </div>
          ))
        : "loading"}
    </div>
  );
}
export default Card;
