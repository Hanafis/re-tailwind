function CardContainer(props){
    return(
        <div className="card card-1 container">
                {props.children}
        </div>
    )
}
export default CardContainer;