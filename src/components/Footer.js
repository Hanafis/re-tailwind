function Footer(){
    return(
        <div>
            <div className="mt-5">
            <div className="br-b mt-5"></div>
            <div className="row center-row mt-5 mb-5 text-grey">
                <span className="mr-3">
                © 2022 Tailwind Labs Inc. All rights reserved.
                </span>
                <a  href="" className="pl-6 br-l text-grey" >Privacy Policy</a>
                </div>
            
            </div>
        </div>
    );
}
export default Footer;