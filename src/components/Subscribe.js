function Subscribe() {
  return (
    <div>
      {/* <img src="/image-footer.jpeg" className="img-footer"/> */}
      
      <div className="container mt-5 card subscribe-baackground hidden-overflow">
        <div className="row mt-5 mb-3">
              <div className="col">
              <div className="absolute inset-0 overflow-hidden rounded-lg">
            <img src="/bg-footer.jpg" alt="" className="absolute bg-footer" />
          </div>
          <div className="col color-white ">
            <h1 className="ml-2 bold">We’ve got more coming...</h1>
            <h3 className="col-6 line-height">
              Want to hear from us when we add new components? Sign up for our
              newsletter and we'll email you every time we release a new batch
              of components.
            </h3>
            <div className="row mt-5 ml-2 mb-5 mr-3 ">
              <div className="col-md-4">
                <input
                  type="text"
                  className="mr-2 form-control-subscribe btn-secondary"
                  size=""
                  height="50px"
                  placeholder="Enter emai address"
                />
              </div>
              <div>
                <button className="btn-subscribe ml-2" ><span className="bold">Subscribe</span></button>
              </div>
            </div>
          </div>
              </div>
        </div>
        <img src="/bg-image-footer.png" className="bg-img-footer"/>
      </div>
    </div>
  );
}
export default Subscribe;
