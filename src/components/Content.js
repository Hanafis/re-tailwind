import { useState } from "react";
import Header from "./Header";
import ContentTwo from "./Content/ContentTwo";
import ContentThreeBadges from "./Content/ContentThreeBadges";
import ContentThreeCard from "./Content/ContentThreeCard";
import Modal from "./Content/Modal";
import ModalTambah from "./Content/ModalTambah";
function Content(props) {
  const [modal,setModal]=useState('hide')
  const [modalTambah,setModalTambah]=useState('hide')
  const [tmppost,setTmpPost]=useState(
    {
      userId: '', 
      id: '', 
      title: '', 
      body: ''
    }
  )
  const getIdData =(d)=>{
    setTmpPost(d)
    setModal('show')
    
  }
  const onBtnTambah=()=>{
    setModalTambah('show')
  }
  const onCloseModalTambah=()=>{
    setModalTambah('hide')
  }
  const onCloseModal=()=>{
    setModal('hide')
  }
  return (
    <div className="container mt-5">
      <div className="row">
        <div className="col">
          <div className="container">
            <Header/>
            <div className="row ml-1">
              <ContentTwo getIdData={getIdData} onBtnTambah={onBtnTambah} deleteData={props.deleteData} posts={props.posts} users={props.users}/>
              <ContentThreeCard getIdData={getIdData} onBtnTambah={onBtnTambah} deleteData={props.deleteData} posts={props.posts} users={props.users}/>
              <ContentThreeBadges getIdData={getIdData} onBtnTambah={onBtnTambah} deleteData={props.deleteData} posts={props.posts} users={props.users}/>
            </div>
          </div>
        </div>
      </div>
      <Modal modal={modal} post={tmppost} onCloseModal={onCloseModal} updateData={props.updateData}/>
      <ModalTambah  ModalTambah={modalTambah} onCloseModalTambah={onCloseModalTambah} tambahData={props.tambahData}/>
    </div>
  );
}
export default Content;
