function Header() {
  return (
    <div>
      <div className="row">
        <a href="#" className="agrey">
          Marketing
        </a>
        <img src="/arrows.svg" className="text-grey"/>
        <a href="#" className="agrey">
          Page Sections
        </a>
      </div>
      <h1>Blog Sections</h1>
    </div>
  );
}
export default Header;
