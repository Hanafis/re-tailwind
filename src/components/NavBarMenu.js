function NavBarMenu(){
    return(
        <nav className="border bg-white">
        <div className="container ">
            <div className="row">
                <div className="col">
                <ul className="center-row">
                <li className="float-left"><a href=""><img src="/logo.png"/></a></li>
                <li className="float-left mt-3 nav-hidden"><a href="#">by the creators of Tailwind CSS</a></li>
                </ul>
                </div>
                <div className="col-md-4 col-6-nav">
                <ul className="center-row">
                <li className="float-right mt-3 pl-6 br-l "><a href="#">Sign in</a></li>
                <li className="float-right mt-3 "><a href="#">Documentation</a></li>
                <li className="float-right mt-3 "><a href="#">Pricing & FAQ</a></li>
                </ul>
                </div>
            </div>
        
        </div>
      
    </nav>
    );
}
export default NavBarMenu;