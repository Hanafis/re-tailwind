import React from 'react';
import { useState, useEffect } from "react";
import './App.css';
import  NavBarMenu  from "./components/NavBarMenu.js";
import  Content  from "./components/Content";
import  Subscribe  from "./components/Subscribe";
import  Footer  from "./components/Footer";

function App() {
  const [post,setPost]=useState([]);
  const getPost=()=>{
    fetch('https://jsonplaceholder.typicode.com/posts'
    ,{
      headers : { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
       }
    }
    )
      .then(function(response){
        console.log(response)
        return response.json();
      })
      .then(function(myJson) {
        console.log("Get Data Post Sucess");
        setPost(myJson.slice(myJson.length - 4))
      });
  }
  useEffect(()=>{
    getPost()
  },[])
  const [user,setUser]=useState([]);
  const getUser=()=>{
    fetch('https://jsonplaceholder.typicode.com/users'
    ,{
      headers : { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
       }
    }
    )
      .then(function(response){
        console.log(response)
        return response.json();
      })
      .then(function(myJson) {
        console.log("Get Data User Sucess");
        setUser(myJson.slice(myJson.length - 4))
      });
  }
  useEffect(()=>{
    getUser()
  },[])
  const tambahData=(data)=>{
      setPost((prevPost)=>{
        return [...prevPost,data]
      })
  }
  const updateData=(data)=>{
    const items=[...post]
    const index = post.findIndex(x => x.id ===data.id);
    const item ={...post[index]}
    item.title=data.title
    item.body=data.body
    items[index]=item
    setPost((prevPost)=>{
        return items
    })
  }
  const deleteData=(data)=>{
    const items=[...post]
    const index = post.findIndex(x => x.id ===data.id);
    items.splice(index,1)
    setPost((prevPost)=>{
      return items
    })
  }
  return (
    <div >
      <NavBarMenu />
      <Content posts={post} users={user} tambahData={tambahData} updateData={updateData} deleteData={deleteData}/>
      <Subscribe   />
      <Footer  />
    </div>
  );
}

export default App;



